﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipesHW
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RecipePage : ContentPage
	{
        ObservableCollection<Recipe> recipeCollection = new ObservableCollection<Recipe>();


        public RecipePage()
		{
			InitializeComponent();

            PopulateRecipeList();
		}

        private void PopulateRecipeList()
        {
            var recipe1 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("sicilianchicken.jpg"),
                RecipeName = "Sicilian Chicken",
                RecipeDetails = "Sicilian Roasted Chicken",
                RecipeURL = "http://allrecipes.com/recipe/255263/sicilian-roasted-chicken/"
            };
            var recipe2 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("jumbalaya.jpg"),
                RecipeName = "Jumbalya",
                RecipeDetails = "Colleen's Slow Cooker Jambalaya",
                RecipeURL = "http://allrecipes.com/recipe/73634/colleens-slow-cooker-jambalaya/"
            };
            var recipe3 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("macandcheese.jpg"),
                RecipeName = "Macaroni and Cheese",
                RecipeDetails = "Simple Macaroni and Cheese",
                RecipeURL = "http://allrecipes.com/recipe/238691/simple-macaroni-and-cheese/"
            };
            var recipe4 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("chickenparm.jpg"),
                RecipeName = "Chicken Parmesan",
                RecipeDetails = "Chicken Parmesan",
                RecipeURL = "http://allrecipes.com/recipe/223042/chicken-parmesan/"
            };
            var recipe5 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("cheesecake.jpg"),
                RecipeName = "Fudge Truffle Cheesecake",
                RecipeDetails = "Fudge Truffle Cheesecake",
                RecipeURL = "http://allrecipes.com/recipe/15917/fudge-truffle-cheesecake/"
            };
            var recipe6 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("cheesedip.jpg"),
                RecipeName = "Jalapeno Cheese Dip",
                RecipeDetails = "Insanely Amazing Jalapeno Cheese Dip",
                RecipeURL = "http://allrecipes.com/recipe/203218/insanely-amazing-jalapeno-cheese-dip/"
            };
            var recipe7 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("beansoup.jpg"),
                RecipeName = "White Bean Soup",
                RecipeDetails = "Creamy Italian White Bean Soup",
                RecipeURL = "http://allrecipes.com/recipe/13287/creamy-italian-white-bean-soup/"
            };
            var recipe8 = new Recipe
            {
                RecipePicture = ImageSource.FromFile("tacosoup.jpg"),
                RecipeName = "Chicken Taco Soup",
                RecipeDetails = "Slow Cooker Chicken Taco Soup",
                RecipeURL = "http://allrecipes.com/recipe/70343/slow-cooker-chicken-taco-soup/"
            };

            recipeCollection.Add(recipe1);
            recipeCollection.Add(recipe2);
            recipeCollection.Add(recipe3);
            recipeCollection.Add(recipe4);
            recipeCollection.Add(recipe5);
            recipeCollection.Add(recipe6);
            recipeCollection.Add(recipe7);
            recipeCollection.Add(recipe8);

            ListViewRecipe.ItemsSource = recipeCollection;
        }

        void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var recipe = (Recipe)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfoPage(recipe));
        }

        private void Handle_DeleteClicked(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var recipe = (Recipe)menuItem.CommandParameter;
            recipeCollection.Remove(recipe);
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            recipeCollection.Clear();
            PopulateRecipeList();

            ListViewRecipe.IsRefreshing = false;
        }
    }
}