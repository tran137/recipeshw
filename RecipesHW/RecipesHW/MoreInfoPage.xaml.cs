﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipesHW
{
    public partial class MoreInfoPage : ContentPage
    {
        String website;

        public MoreInfoPage()
        {
            InitializeComponent();
        }

        public MoreInfoPage(Recipe recipe)
        {
            InitializeComponent();
            BindingContext = recipe;
            website = recipe.RecipeURL;
        }

        void OnWebsiteClicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri(website));
        }
    }

}