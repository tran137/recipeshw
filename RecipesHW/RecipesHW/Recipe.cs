﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RecipesHW
{
    public class Recipe
    {
        public string RecipeName
        {
            get;
            set;
        }

        public string RecipeDetails
        {
            get;
            set;
        }

        public ImageSource RecipePicture
        {
            get;
            set;
        }

        public string RecipeURL
        {
            get;
            set;
        }

    }
}
